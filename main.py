import os
import shutil
import cv2

img_dir_path = "/Users/ihyeon-u/Downloads/LeeProject/data/dog_nose_print_image_pool_cropped"
result_img_dir_path = "/Users/ihyeon-u/Downloads/LeeProject/data/dog_nose_print_image_pool_cropped_jpg"


def main_stream():
    if os.path.exists(result_img_dir_path):
        shutil.rmtree(result_img_dir_path)

    os.makedirs(result_img_dir_path)

    # 파일 리스트 가져오기
    print("이미지 리스트 작성 중")
    possible_img_extension = ['.jpg', '.jpeg', '.JPG', '.bmp', '.png', '.PNG', 'BMP']
    img_path_list = []

    for (root, dirs, files) in os.walk(img_dir_path):
        if len(files) > 0:
            for file_name in files:
                if os.path.splitext(file_name)[1] in possible_img_extension:
                    img_path = root + '/' + file_name
                    img_path = img_path.replace('\\', '/')  # \는 \\로 나타내야함
                    img_path_list.append(img_path)

    print(len(img_path_list))
    print("data 복사 중")

    # 이미지 인덱스
    imgidx = 0
    result_img_path_list = []
    for img_file_name in img_path_list:
        if imgidx % 10 == 0:
            print("진행중 : " + str(imgidx))

        imgidx += 1

        # 확장자 분리
        file_name_with_ext = img_file_name.split("/")[-1]
        file_exp = file_name_with_ext.split(".")[-1]
        file_name = file_name_with_ext.split(".")[-2]

        result_file_name = get_result_file_name(result_img_path_list, result_img_dir_path + "/" + file_name + ".jpg")

        image = cv2.imread(img_file_name)
        cv2.imwrite(result_file_name, image,
                    [int(cv2.IMWRITE_JPEG_QUALITY), 100])

        result_img_path_list.append(result_file_name)

    print("data 복사 완료")


def get_result_file_name(result_img_path_list, result_file_name):
    idx = 1

    while True:
        if result_img_path_list.__contains__(result_file_name):
            file_exp = result_file_name.split(".")[-1]
            file_name = result_file_name.split(".")[-2]
            new_result_file_name = file_name + "(" + str(idx) + ")."
            result_name = new_result_file_name + file_exp

            result_file_name = result_name
            idx = idx + 1
        else:
            break

    return result_file_name


if __name__ == '__main__':
    main_stream()
